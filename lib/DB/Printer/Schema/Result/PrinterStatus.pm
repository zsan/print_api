use utf8;
package DB::Printer::Schema::Result::PrinterStatus;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::Printer::Schema::Result::PrinterStatus

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("TimeStamp");

=head1 TABLE: C<printer_status>

=cut

__PACKAGE__->table("printer_status");

=head1 ACCESSORS

=head2 printer_status_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 45

=cut

__PACKAGE__->add_columns(
  "printer_status_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</printer_status_id>

=back

=cut

__PACKAGE__->set_primary_key("printer_status_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<name>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("name", ["name"]);


# Created by DBIx::Class::Schema::Loader v0.07017 @ 2013-11-22 14:27:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vJuI4BR6z/N2QAefsmrcUg


# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->belongs_to(printer => 'DB::Printer::Schema::Result::Printer' => 'printer_status_id');
1;
