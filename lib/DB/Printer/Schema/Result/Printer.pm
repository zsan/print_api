use utf8;
package DB::Printer::Schema::Result::Printer;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::Printer::Schema::Result::Printer

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("TimeStamp");

=head1 TABLE: C<printer>

=cut

__PACKAGE__->table("printer");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 45

=head2 node_id

  data_type: 'integer'
  is_nullable: 0

=head2 printer_status_id

  data_type: 'integer'
  is_nullable: 0

=head2 dt_status_changed

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 printer_model_id

  data_type: 'integer'
  is_nullable: 0

=head2 dt_created

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "node_id",
  { data_type => "integer", is_nullable => 0 },
  "printer_status_id",
  { data_type => "integer", is_nullable => 0 },
  "dt_status_changed",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "printer_model_id",
  { data_type => "integer", is_nullable => 0 },
  "dt_created",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<node_id>

=over 4

=item * L</node_id>

=back

=cut

__PACKAGE__->add_unique_constraint("node_id", ["node_id"]);


# Created by DBIx::Class::Schema::Loader v0.07017 @ 2013-11-22 13:10:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rkntt7NeA7O+qBy9ZOzMiQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->add_columns(
  'dt_created', {
    data_type     => 'timestamp',
    set_on_create => 1
  },
  'dt_status_changed', {
    data_type     => 'timestamp',
    set_on_create => 1,
    set_on_update => 1
  }
);

__PACKAGE__->has_one(
  "printer_model", 'DB::Printer::Schema::Result::PrinterModel',
  { "foreign.printer_model_id" => "self.printer_model_id"}
);

__PACKAGE__->has_one(
  "printer_status", 'DB::Printer::Schema::Result::PrinterStatus',
  { "foreign.printer_status_id" => "self.printer_status_id"});

1;
