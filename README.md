PRINTER API DEMO
================

INSTALL
--------
- get the source code via gitlab

  ```perl
  git clone https://gitlab.com/zsan/print_api.git
  ````

- cd to app directory

  ```perl
  cd print_api
  ````

- install dependencies
  
  ```sh
  sudo cpanm --installdeps .
  ```

- dump schema

  ```sh
  mysql -u username -p password -h host printer_api < sql/schema.sql
  ```


Run the web app
---------------

- cd to app directory

  ```sh
  cd print_api
  ```

- run it with morbo

  ```sh
  morbo print_api.pl
  ```


And try to get request to devel machine:

```sh
	http://localhost:3000/api/printer/assign_order?printer_id=1a1&print_order_id=1a
````

```sh
	http://localhost:3000/api/printer/assign_order?printer_id=11&print_order_id=1
```


Upload to Heroku
----------------

- Create heroku app:

  ```sh
  	heroku create -s cedar --buildpack http://github.com/judofyr/perloku.git
  ```

- And run this command:

  ```sh
  	git push heroku master
  ```


Live Demo
---------
```sh
http://serene-wave-9103.herokuapp.com/api/printer/assign_order?printer_id=11&print_order_id=1
```


Simple Test
-----------
```perl
	prove -v -c
```

Simple test file are under directory `t`
