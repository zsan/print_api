use Test::More;
use Test::Mojo;
use FindBin qw/$Bin/;
require "$Bin/../print_api.pl";

my $t = Test::Mojo->new();

$t->get_ok("/api/printer/assign_order")
  ->status_is(200)
  ->content_type_is('application/json')
  ->json_is("/status/error/print_order_id/" => "this field is required")
  ->json_is("/status/error/printer_id/"     => "this field is required");
  
$t->get_ok("/api/printer/assign_order" => 
    form => {print_order_id => 1, printer_id=> 1})
  ->status_is(200)
  ->content_type_is('application/json')
  ->json_hasnt("/status/error/print_order_id/")
  ->json_hasnt("/status/error/printer_id/");

done_testing();