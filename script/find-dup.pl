#!/usr/bin/env perl
use Modern::Perl;
use File::Find::Duplicates;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use POSIX qw/strftime/;

# Hardcoded directory, in real application we can use Getopt::Long
# so we can pass argument to the script like:
# perl script.pl --directory /tmp --zip-output /home/directory
my $dir   = '/tmp/';

# Use module to find duplicated files, its easier and keep our live happier
my @dupes = find_duplicate_files($dir);

# Push files to array so we can archived later
# $_->files contains duplicated files, and we only pick 1 file to be archived
my @should_archived = map { $_->files->[0] } @dupes;

# Archive::Zip's object
my $zip = Archive::Zip->new;

# add files to the object
$zip->addFile($_) for @should_archived;

# create unique filename based on timestamp
my $filename = strftime('%Y_%m_%d_%H_%M_%S', localtime) . ".zip";

# Zip all files into zipped file in the same directory ($dir)
die "write error" unless $zip->writeToFileNamed($dir . $filename) == AZ_OK;

# upload to server (Its fake)
my $upload_success = upload_to_server();

# Uncomment the code if you're really want to remove all files
# I commented the line, in case you want to see the zipped files
# unlink glob "$dir/*.txt" if $upload_success;

sub upload_to_server {
  # i am not sure how the application going to upload to another server
  # could be sftp, ftp or any other approach, so for this case i leave it
  # to fake response
  1;
}
