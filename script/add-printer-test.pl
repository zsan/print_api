#!/usr/bin/env perl
use Modern::Perl;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";
use DB::Printer::Schema;
use List::Util qw/shuffle/;

# Description:
# A simple Demo about how to use DBIx::Class in very basic usage
# The idea is:
# printer has_one printer_model
# printer has_one printer_status

# Usually the table names should be written as 'printers', 'printer_models' and
# printer_statuses, but it just a demo, so let's keep it simple.

# Connect To DSN, in real case we should put DB information in YAML config
# and load with YAML
my $schema = DB::Printer::Schema->connect(
  'dbi:mysql:printer_api:localhost.localdomain', 'root', 'metal666'
);

# Just a shortcuts to tables
my $tbl_printer = $schema->resultset('Printer');
my $tbl_model   = $schema->resultset('PrinterModel');
my $tbl_status  = $schema->resultset('PrinterStatus');


# create 5 printer's models
$tbl_model->create({printer_maker_id => $_, name => "model $_"}) for 1..5;

# create 2 printer's statuses
$tbl_status->create({name => $_}) for qw/inactive active/;

# create 5 rows to table.printer
for (1..5) {
  my @shuffle = shuffle 1..2;
  my $new_printer = {
    name              => "printer $_",
    node_id           => $_,
    printer_status_id => $shuffle[0],
    printer_model_id  => $_
  };

  $tbl_printer->create($new_printer);
}

# Search for printers
my $rs = $tbl_printer->search();

# If we want to dump to csv then use Text::CSV_XS

# print header
say join ",", (qw/PID name model_name status/);

# iterate (3 tables are involved)
while(my $row = $rs->next) {
  say join ",", (
    $row->id, 
    $row->name, 
    $row->printer_model->name,
    $row->printer_status->name
  );
}